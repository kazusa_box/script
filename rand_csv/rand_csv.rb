require 'csv'
require 'optparse'

input = ARGV
csv_pass = input[0] || "./sht_character.csv"
csv_data = CSV.read(csv_pass, headers: true)

rand = rand(0..csv_data.count - 1)

data = csv_data[rand]

puts "#{data['title']}: #{data['name']}"
